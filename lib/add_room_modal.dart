import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'api_service.dart';

class AddRoomModal extends StatefulWidget {
  final VoidCallback onClose;

  AddRoomModal({required this.onClose});

  @override
  _AddRoomModalState createState() => _AddRoomModalState();
}

class _AddRoomModalState extends State<AddRoomModal> {
  final TextEditingController _topicController = TextEditingController();
  String _roomType = 'public';
  bool _isLoading = false;

  void _handleCreateRoom() async {
    if (_topicController.text.isEmpty) return;

    setState(() {
      _isLoading = true;
    });

    try {
      final apiService = Get.find<ApiService>();
      final response = await apiService.createRoom({
        'topic': _topicController.text,
        'roomType': _roomType,
      });
      print("responsebodyresponsebody ${response.body}");
      if (response.statusCode == 200) {
        Get.back();
        widget.onClose();
        Get.snackbar('Success', 'Room created successfully!');
      } else {
        throw Exception('Failed to create room');
      }
    } catch (error) {
      Get.snackbar('Error', 'Failed to create room');
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        padding: EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextField(
              controller: _topicController,
              decoration: InputDecoration(labelText: 'Enter the topic to be discussed'),
            ),
            SizedBox(height: 10),
            Text('Room type'),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ChoiceChip(
                  label: Text('Public'),
                  selected: _roomType == 'public',
                  onSelected: (bool selected) {
                    setState(() {
                      _roomType = 'public';
                    });
                  },
                ),
              ],
            ),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: _isLoading ? null : _handleCreateRoom,
              child: _isLoading ? CircularProgressIndicator() : Text("Let's go"),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'api_service.dart';
import 'rooms_screen.dart';
import 'webrtc_service.dart';
import 'room_detail_screen.dart';
import 'login_screen.dart';

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await requestPermissions();
  final apiService = ApiService();
  await apiService.loadToken();
  await apiService.loadUserDetails();
  Get.put(apiService); // Dependency injection
  runApp(MyApp());
}

Future<void> requestPermissions() async {
  await [
    Permission.microphone,
  ].request();
}

class MyApp extends StatelessWidget {
   final apiService = Get.find<ApiService>();
  @override
  Widget build(BuildContext context) {

    return GetMaterialApp(
      navigatorKey: navigatorKey,
      title: 'Room App',
      initialRoute: '/',
      getPages: [
        GetPage(
          name: '/',
          page:()=>apiService.userDetails==null?LoginScreen():RoomsScreen(),
        ),
        GetPage(name: '/login', page: () => LoginScreen()),
        GetPage(name: '/rooms', page: () => RoomsScreen()),
        GetPage(
          name: '/room',
          page: () => RoomDetailScreen(
            roomId: Get.arguments as String,
            userDetails: Get.find<ApiService>().userDetails!,
          ),
        ),
      ],
    );
  }
}

class Actions {
  static const String JOIN = "join";
  static const String LEAVE = "leave";
  static const String ADD_PEER = "add-peer";
  static const String REMOVE_PEER = "remove-peer";
  static const String RELAY_ICE = "relay-ice";
  static const String RELAY_SDP = "relay-sdp";
  static const String SESSION_DESCRIPTION = "session-description";
  static const String ICE_CANDIDATE = "ice-candidate";
  static const String MUTE = "mute";
  static const String UNMUTE = "unmute";
  static const String MUTE_INFO = "mute-info";
  static const String END_ROOM = "end_room";
  static const String BLOCK_USER = "block_user";
  static const String ROOM_ENDED = "room_ended";
  static const String ROOM_CLIENTS = "room_clients";
  static const String ROOM_ENDED_REDIRECT = "room_ended_redirect";
  static const String RAISE_HAND = "raise_hand";
  static const String APPROVE_SPEAK = "approve_speak";
  static const String REJECT_SPEAK = "reject_speak";
}

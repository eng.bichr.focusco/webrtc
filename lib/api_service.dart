import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class ApiService {
  final String _baseUrl = "https://sared.online:5444/api";
  String? _token;
  Map<String, dynamic>? userDetails;

  ApiService() {
    loadToken();
    loadUserDetails();
  }

  Future<void> loadToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _token = prefs.getString('token');
  }

  Future<void> _saveToken(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('token', token);
  }

  Future<void> loadUserDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final userDetailsString = prefs.getString('userDetails');
    if (userDetailsString != null) {
      userDetails = jsonDecode(userDetailsString);
    }
  }

  Future<void> _saveUserDetails(Map<String, dynamic> details) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    details['role'] = 'admin';
    await prefs.setString('userDetails', jsonEncode(details));
  }

  Future<void> login(String email, String password) async {
    print("$email");
    print("$password");
    final response = await http.post(
      Uri.parse("$_baseUrl/user/login"),
      headers: {
        'Content-Type': 'application/json',
        "Access-Control-Allow-Origin": "*", 
        "Access-Control-Allow-Credentials":
            "true", 
        "Access-Control-Allow-Headers":
            "Origin,Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,locale",
        "Access-Control-Allow-Methods": "POST, OPTIONS"
      },
      body: jsonEncode({
        "email": email,
        "password": password,
      }),
    );

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      _token = data['token'];
      userDetails = data;
      await _saveToken(_token!);
      await _saveUserDetails(userDetails!);
    } else {
      throw Exception('Failed to login');
    }
  }

  Future<void> logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove('token');
    await prefs.remove('userDetails');
    _token = null;
    userDetails = null;
  }

  Future<http.Response> createRoom(Map<String, dynamic> roomDetails) {
    return http.post(
      Uri.parse("$_baseUrl/rooms/createRoom"),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $_token',
      },
      body: jsonEncode(roomDetails),
    );
  }

  Future<http.Response> getAllRooms() {
    return http.post(
      Uri.parse("$_baseUrl/rooms/allRooms"),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $_token',
      },
    );
  }

  Future<http.Response> getRoom(String roomId) {
    return http.get(
      Uri.parse("$_baseUrl/rooms/room/$roomId"),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $_token',
      },
    );
  }

  Map<String, dynamic>? getUserDetails() {
    return userDetails;
  }
}

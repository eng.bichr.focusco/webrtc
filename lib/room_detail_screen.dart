import 'package:flutter/material.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:get/get.dart';
import 'webrtc_service.dart';

class RoomDetailScreen extends StatefulWidget {
  final String roomId;
  final Map<String, dynamic> userDetails;

  RoomDetailScreen({required this.roomId, required this.userDetails});

  @override
  State<RoomDetailScreen> createState() => _RoomDetailScreenState();
}

class _RoomDetailScreenState extends State<RoomDetailScreen> {
  late RTCVideoRenderer _localRenderer;
  late RTCVideoRenderer _remoteRenderer;

  @override
  void initState() {
    super.initState();
    _localRenderer = RTCVideoRenderer();
    _remoteRenderer = RTCVideoRenderer();
    _localRenderer.initialize();
    _remoteRenderer.initialize();
    final WebRTCController webRTCController = Get.put(WebRTCController());
    webRTCController.initialize(widget.roomId, widget.userDetails);
    // ربط التدفقات بـ RTCVideoRenderer
    webRTCController.localRenderer = _localRenderer;
    // webRTCController.remoteRenderers = _remoteRenderer;
  }

  @override
  void dispose() {
    _localRenderer.dispose();
    _remoteRenderer.dispose();
    final WebRTCController webRTCController = Get.find<WebRTCController>();
    webRTCController.cleanupConnections();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    final WebRTCController webRTCController = Get.find<WebRTCController>();

    return WillPopScope(
      onWillPop: () async {
        webRTCController.cleanupConnections();
        Get.offAllNamed('/rooms');
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text('Room Details'),
          actions: [
            IconButton(
              icon: Icon(Icons.share),
              onPressed: () {
                webRTCController.handleShare();
              },
            ),
            IconButton(
              icon: Icon(Icons.content_copy),
              onPressed: webRTCController.copyURL,
            ),
            Obx(() {
              if (webRTCController.isAdmin.value) {
                return IconButton(
                  icon: Icon(Icons.close),
                  onPressed: webRTCController.endRoom,
                );
              }
              return Container();
            }),
          ],
        ),
        body: Obx(() {
          if (webRTCController.clients.isEmpty) {
            return Center(child: CircularProgressIndicator());
          } else {
            return Column(
              children: [
                Expanded(
                  child: ListView.builder(
                    itemCount: webRTCController.clients.length,
                    itemBuilder: (context, index) {
                      final client = webRTCController.clients[index];
                      return ListTile(
                        leading: CircleAvatar(
                          backgroundImage: NetworkImage(client['profile']),
                        ),
                        title: Text(client['username'] ?? 'Unknown'),
                        subtitle: Row(
                          children: [
                            Text(client['role'] ?? 'Unknown role'),
                            client['muted'] == true
                                ? IconButton(
                                    onPressed: () {
                                      webRTCController.setUnMute(client['_id'], client['peerId']);
                                    },
                                    icon: Icon(Icons.mic_off),
                                  )
                                : IconButton(
                                    onPressed: () {
                                      webRTCController.setMute(client['_id'], client['peerId']);
                                    },
                                    icon: Icon(Icons.mic),
                                  ),
                          ],
                        ),
                        trailing: webRTCController.isAdmin.value
                            ? PopupMenuButton<String>(
                                onSelected: (value) {
                                  if (value == 'Block') {
                                    webRTCController.blockUser(client['_id']);
                                  }
                                },
                                itemBuilder: (context) => [
                                  PopupMenuItem(
                                    value: 'Block',
                                    child: Text('Block'),
                                  ),
                                ],
                              )
                            : null,
                      );
                    },
                  ),
                ),
                // Expanded(
                //   child: Column(
                //     children: [
                //       Expanded(
                //         child: RTCVideoView(_localRenderer, mirror: true),
                //       ),
                //       Expanded(
                //         child: RTCVideoView(_remoteRenderer),
                //       ),
                //     ],
                //   ),
                // ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FloatingActionButton(
                    onPressed: webRTCController.toggleMute,
                    child: Obx(() => Icon(
                      webRTCController.isMuted.value
                          ? Icons.mic_off
                          : Icons.mic,
                    )),
                  ),
                ),
              ],
            );
          }
        }),
      ),
    );
  }
}



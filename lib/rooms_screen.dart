import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'api_service.dart';
import 'room_detail_screen.dart';
import 'add_room_modal.dart';

class RoomsController extends GetxController {
  final ApiService apiService = Get.find();
  TextEditingController searchController = TextEditingController();
  var rooms = [].obs;
  var isLoading = true.obs;

  @override
  void onInit() {
    super.onInit();
    fetchRooms();
  }

  void fetchRooms() async {
    isLoading.value = true;
    try {
      final response = await apiService.getAllRooms();
      if (response.statusCode == 200) {
        rooms.value = List.from(jsonDecode(response.body));
      } else {
        throw Exception('Failed to load rooms');
      }
    } catch (error) {
      Get.snackbar("Error", "Failed to load rooms", snackPosition: SnackPosition.BOTTOM);
    } finally {
      isLoading.value = false;
    }
  }

  void addRoom() {
    Get.dialog(AddRoomModal(onClose: fetchRooms));
  }
}

class RoomsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final controller = Get.put(RoomsController());

    return Scaffold(
      appBar: AppBar(
        title: Text('Rooms'),
        actions: [
          IconButton(
            icon: Icon(Icons.add),
            onPressed: controller.addRoom,
          ),
            IconButton(
            icon: Icon(Icons.refresh),
            onPressed: controller.fetchRooms,
          ),
        ],
      ),
      body: Obx(() {
        if (controller.isLoading.value) {
          return Center(child: CircularProgressIndicator());
        } else {
          return Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: TextField(
                  controller: controller.searchController,
                  decoration: InputDecoration(
                    hintText: 'Search',
                    prefixIcon: Icon(Icons.search),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  onChanged: (value) {
                    controller.searchController.text = value;
                    controller.searchController.selection = TextSelection.fromPosition(
                      TextPosition(offset: controller.searchController.text.length),
                    );
                  },
                ),
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: controller.rooms.length,
                  itemBuilder: (context, index) {
                    final room = controller.rooms[index];
                    if (controller.searchController.text.isEmpty ||
                        room['topic'].toLowerCase().contains(controller.searchController.text.toLowerCase())) {
                      return ListTile(
                        title: Text(room['topic']),
                        onTap: () async {
                          Get.to(() => RoomDetailScreen(
                            roomId: room['_id'],
                            userDetails: controller.apiService.userDetails!,
                          ));
                          controller.fetchRooms();
                        },
                      );
                    } else {
                      return Container();
                    }
                  },
                ),
              ),
            ],
          );
        }
      }),
    );
  }
}

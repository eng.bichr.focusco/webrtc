import 'package:socket_io_client/socket_io_client.dart' as IO;

class SocketService {
  IO.Socket? socket;

  void initSocket() {
    socket = IO.io('https://sared.online:5444', <String, dynamic>{
      'transports': ['websocket'],
      'forceNew': true,
      'reconnectionAttempts': 'Infinity',
      'timeout': 10000,
    });

    socket!.on('connect_error', (err) {
      print('WebSocket connection error: $err');
    });

    socket!.on('connect', (_) {
      print('Connected to WebSocket server');
    });

    socket!.on('disconnect', (_) {
      print('Disconnected from WebSocket server');
    });

    socket!.on('reconnect_attempt', (_) {
      print('Attempting to reconnect to WebSocket server');
    });

    socket!.on('reconnect_failed', (_) {
      print('Failed to reconnect to WebSocket server');
    });

    socket!.on('reconnect_error', (error) {
      print('Reconnection error: $error');
    });
  }
}

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart' as getx;
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'package:clipboard/clipboard.dart';
import 'package:share_plus/share_plus.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_sound/flutter_sound.dart';
import 'dart:io';

class WebRTCController extends getx.GetxController {
  late String roomId;
  late Map<String, dynamic> userDetails;
  late IO.Socket socket;
  MediaStream? localStream;
  Map<String, RTCPeerConnection> peerConnections = {};
  Map<String, MediaStream> remoteStreams = {};
  Map<String, RTCVideoRenderer> remoteRenderers = {};
  var clients = <Map<String, dynamic>>[].obs;
  var isMuted = true.obs;
  var isAdmin = false.obs;
  var handRaiseRequests = <Map<String, dynamic>>[].obs;
  RTCVideoRenderer localRenderer = RTCVideoRenderer();
  FlutterSoundRecorder recorder = FlutterSoundRecorder();
  bool isRecording = false;
  String? filePath;

  final List<Map<String, dynamic>> iceServers = [
    {'urls': 'stun:stun.l.google.com:19302'},
    {'urls': 'stun:stun1.l.google.com:19302'},
    {'urls': 'stun:stun2.l.google.com:19302'},
    {'urls': 'stun:stun3.l.google.com:19302'},
    {'urls': 'stun:stun4.l.google.com:19302'},
    {'urls': 'stun:stun.services.mozilla.com'}
  ];

  @override
  void onInit() {
    super.onInit();
    localRenderer.initialize();
  }

  void initialize(String roomId, Map<String, dynamic> userDetails) async {
    this.roomId = roomId;
    this.userDetails = userDetails;
    isAdmin.value = userDetails['role'] == 'admin';
    await _requestPermissions();
    await _initLocalStream();
    _initSocket();
  }

  Future<void> _requestPermissions() async {
    final status = await Permission.microphone.request();
    if (status.isGranted) {
      Fluttertoast.showToast(
        msg: "Microphone permission granted.",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.green,
        textColor: Colors.white,
        fontSize: 16.0,
      );
    } else if (status.isDenied || status.isPermanentlyDenied) {
      Fluttertoast.showToast(
        msg: "Microphone permission is required to join the room.",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0,
      );
      if (status.isPermanentlyDenied) {
        openAppSettings();
      }
    }
  }

  Future<void> _initLocalStream() async {
    if (!await Permission.microphone.isGranted) {
      await _requestPermissions();
      if (!await Permission.microphone.isGranted) {
        Fluttertoast.showToast(
          msg: "Microphone permission is required to join the room.",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0,
        );
        return;
      }
    }

    try {
      final mediaConstraints = {
        'audio': true,
      };
      localStream = await navigator.mediaDevices.getUserMedia(mediaConstraints);
      if (kDebugMode) {
        print("localStream: $localStream");
      }
      if (localStream != null) {
        localRenderer.srcObject = localStream;
        localStream?.getAudioTracks().forEach((track) {
          track.enabled = true;
        });
        isMuted.value = false;

        peerConnections.forEach((key, pc) {
          localStream?.getTracks().forEach((track) {
            pc.addTrack(track, localStream!);
          });
        });
        update();
      } else {
        throw Exception('Local stream is null');
      }
    } catch (e) {
      print("Error initializing local stream: $e");
      Fluttertoast.showToast(
        msg: "Failed to initialize microphone: $e",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0,
      );
    }
  }

  Future<void> startRecording(BuildContext context) async {
    bool hasPermission = await _checkMicrophonePermission();
    if (hasPermission) {
      if (!recorder.isRecording) {
        final directory = await getApplicationDocumentsDirectory();
        var filePath = '${directory.path}/recording.wav';
        await _initRecorder();
        await recorder.startRecorder(
          toFile: filePath,
          codec: Codec.pcm16WAV,
        );
        isRecording = true;
        this.filePath = filePath;
        update();
      } else {
        await recorder.stopRecorder();
        final directory = await getApplicationDocumentsDirectory();
        var filePath = '${directory.path}/recording.wav';
        final bytes = await File(filePath).readAsBytes();
        final newFilePath = '${directory.path}/new_recording.wav';
        final file = await File(newFilePath).writeAsBytes(bytes);
        isRecording = false;
        this.filePath = newFilePath;
        update();
      }
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            child: Text('We use permission to send audio recording to the driver'.tr),
          );
        }
      );
    }
  }

  Future<void> _initRecorder() async {
    await recorder.openRecorder();
  }

  Future<bool> _checkMicrophonePermission() async {
    var status = await Permission.microphone.status;
    if (!status.isGranted) {
      status = await Permission.microphone.request();
    }
    return status.isGranted;
  }

  void _initSocket() {
    socket = IO.io('wss://sared.online:5444', <String, dynamic>{
      'transports': ['websocket'],
      'autoConnect': false,
    });

    socket.onConnect((_) {
      _joinRoom();
    });

    socket.onDisconnect((_) {
      cleanupConnections();
    });

    socket.on('room_clients', (data) {
      clients.value = List<Map<String, dynamic>>.from(data['clients']);
    });

    socket.on('unmute', (data) {
      setUnMute(data['userId'], data['peerId']);
    });

    socket.on('mute', (data) {
      setMute(data['userId'], data['peerId']);
    });

    socket.on('ROOM_ENDED_REDIRECT', (_) {
      Fluttertoast.showToast(
        msg: "Room ended",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0,
      );
      getx.Get.offAllNamed('/rooms');
    });

    socket.on('add-peer', _handleNewPeer);
    socket.on('remove-peer', _handleRemovePeer);
    socket.on('ice-candidate', _handleIceCandidate);
    socket.on('session-description', _setRemoteDescription);
    socket.on('RAISE_HAND', _handleRaiseHand);
    socket.on('REJECT_SPEAK', _handleRejectSpeak);
    socket.on('APPROVE_SPEAK', _handleApproveSpeak);
    socket.on('RAISE_HAND_DUPLICATE', _handleRaiseHandDuplicate);
    socket.connect();
  }

  void _joinRoom() {
    socket.emit('join', {'roomId': roomId, 'user': userDetails});
  }

  void endRoom() {
    socket.emit('end_room', roomId);
  }

  Future<void> _cleanUp() async {
    for (var connection in peerConnections.values) {
      connection.close();
    }
    peerConnections.clear();
    await localStream?.dispose();
  }

  void _handleNewPeer(dynamic data) async {
    String peerId = data['peerId'];
    if (peerConnections.containsKey(peerId)) {
      print('You are already connected with $peerId');
      return;
    }

    var configuration = {
      'iceServers': iceServers,
    };

    var connection = await createPeerConnection(configuration);
    peerConnections[peerId] = connection;

    connection.onIceCandidate = (RTCIceCandidate candidate) {
      print("Generated ICE candidate: ${candidate.toMap()}");
      socket.emit('ice-candidate', {
        'peerId': peerId,
        'candidate': candidate.toMap(),
      });
    };

    connection.onTrack = (RTCTrackEvent event) {
      print("Track event received: ${event.track.kind}");
      if (event.track.kind == 'audio') {
        _handleRemoteAudioTrack(peerId, event.streams.first);
      }
    };

    connection.onConnectionState = (RTCPeerConnectionState state) {
      print("Connection state changed: $state");
      if (state == RTCPeerConnectionState.RTCPeerConnectionStateFailed) {
        print("Connection failed for peerId: $peerId");
        _handleConnectionFailed(peerId);
      }
    };

    connection.onIceConnectionState = (RTCIceConnectionState state) {
      print("ICE connection state changed: $state");
      if (state == RTCIceConnectionState.RTCIceConnectionStateFailed) {
        print("ICE connection failed for peerId: $peerId");
        _handleConnectionFailed(peerId);
      }
    };

    if (localStream != null) {
      localStream!.getTracks().forEach((track) {
        connection.addTrack(track, localStream!);
      });
    } else {
      print('Local stream is not available.');
    }

    if (data['createOffer']) {
      try {
        RTCSessionDescription offer = await connection.createOffer();
        await connection.setLocalDescription(offer);
        socket.emit('session-description', {
          'peerId': peerId,
          'sessionDescription': offer.toMap(),
        });
      } catch (error) {
        print('Error creating offer: $error');
      }
    }
  }

  void _handleRemoteAudioTrack(String peerId, MediaStream stream) async {
    print('Handling remote audio track for peerId: $peerId');
    remoteStreams[peerId] = stream;

    // Create a new renderer for this peer
    RTCVideoRenderer renderer = RTCVideoRenderer();
    await renderer.initialize();
    renderer.srcObject = stream;
    remoteRenderers[peerId] = renderer;

    print('Handling remote audio track for peerId: ${renderer.srcObject}');
  }

  void _handleRemovePeer(dynamic data) {
    String peerId = data['peerId'];
    peerConnections[peerId]?.close();
    peerConnections.remove(peerId);
    remoteStreams.remove(peerId);
    remoteRenderers[peerId]?.dispose();
    remoteRenderers.remove(peerId);
  }

  void _handleIceCandidate(dynamic data) async {
    print("handleIceCandidate: $data");

    String peerId = data['peerId'];
    var candidateData = data['icecandidate'];

    if (candidateData == null) {
      print("ICE candidate data is null for peer: $peerId");
      return;
    }

    RTCIceCandidate candidate = RTCIceCandidate(
      candidateData['candidate'],
      candidateData['sdpMid'],
      candidateData['sdpMLineIndex'],
    );

    var peerConnection = peerConnections[peerId];
    if (peerConnection != null) {
      await peerConnection.addCandidate(candidate);
      print("Added ICE candidate for peer: $peerId");
    } else {
      print("Peer connection not found for peer: $peerId");
    }
  }

  Future<void> _setRemoteDescription(dynamic data) async {
    print("setRemoteDescription${data}");
    String peerId = data['peerId'];
    RTCSessionDescription description = RTCSessionDescription(
      data['sessionDescription']['sdp'],
      data['sessionDescription']['type'],
    );

    var peerConnection = peerConnections[peerId];
    if (peerConnection != null) {
      await peerConnection.setRemoteDescription(description);
      if (description.type == 'offer') {
        RTCSessionDescription answer = await peerConnection.createAnswer();
        await peerConnection.setLocalDescription(answer);
        socket.emit('session-description', {
          'peerId': peerId,
          'sessionDescription': answer.toMap(),
        });
      }
    } else {
      print("Cannot set remote description, peerConnection is null for peerId: $peerId");
    }
  }

  void _handleConnectionFailed(String peerId) {
    // Handle connection failure logic
    Fluttertoast.showToast(
      msg: "Connection failed for peerId: $peerId",
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.CENTER,
      backgroundColor: Colors.red,
      textColor: Colors.white,
      fontSize: 16.0,
    );
    peerConnections[peerId]?.close();
    peerConnections.remove(peerId);
    remoteStreams.remove(peerId);
    remoteRenderers[peerId]?.dispose();
    remoteRenderers.remove(peerId);
    update();
  }

  void setMute(String userId, String peerId) {
    int index = clients.indexWhere((client) => client['_id'] == userId);
    if (index != -1) {
      clients[index]['muted'] = true;
    }
  }

  void setUnMute(String userId, String peerId) {
    int index = clients.indexWhere((client) => client['_id'] == userId);
    if (index != -1) {
      clients[index]['muted'] = false;
    }
  }

  void toggleMute() {
    isMuted.value = !isMuted.value;
    localStream?.getAudioTracks().forEach((track) {
      track.enabled = !isMuted.value;
    });
    socket.emit(isMuted.value ? 'mute' : 'unmute',
        {'roomId': roomId, 'userId': userDetails['_id']});
  }

  void blockUser(String userId) {
    socket.emit('block_user', {'roomId': roomId, 'userId': userId});
  }

  void handleShare() {
    final roomURL = 'https://sared.online/room/$roomId';
    Share.share('Join the room using this link: $roomURL');
  }

  void copyURL() {
    final roomURL = 'https://sared.online/room/$roomId';
    FlutterClipboard.copy(roomURL).then((value) {
      Fluttertoast.showToast(
        msg: "Room URL copied to clipboard.",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.green,
        textColor: Colors.white,
        fontSize: 16.0,
      );
    });
  }

  void _handleRaiseHand(dynamic data) {
    handRaiseRequests.add({
      'peerId': data['peerId'],
      'userId': data['userId'],
      'username': data['username'],
      'profile': data['profile'],
    });
    Fluttertoast.showToast(
      msg: "User ${data['userId']} has raised their hand.",
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.blue,
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }

  void _handleRejectSpeak(dynamic data) {
    Fluttertoast.showToast(
      msg: "User ${data['userId']} has been rejected to speak.",
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.red,
      textColor: Colors.white,
      fontSize: 16.0,
    );
    handRaiseRequests
        .removeWhere((request) => request['userId'] == data['userId']);
  }

  void _handleApproveSpeak(dynamic data) {
    Fluttertoast.showToast(
      msg: "User ${data['userId']} has been approved to speak.",
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.green,
      textColor: Colors.white,
      fontSize: 16.0,
    );
    handRaiseRequests
        .removeWhere((request) => request['userId'] == data['userId']);
  }

  void _handleRaiseHandDuplicate(dynamic data) {
    Fluttertoast.showToast(
      msg: data['message'],
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.yellow,
      textColor: Colors.black,
      fontSize: 16.0,
    );
  }

  void raiseHand() {
    if (handRaiseRequests.any((request) => request['userId'] == userDetails['_id'])) {
      Fluttertoast.showToast(
        msg: "You have already raised your hand. Please wait for the admin to approve or reject.",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.orange,
        textColor: Colors.white,
        fontSize: 16.0,
      );
      return;
    }

    socket.emit('RAISE_HAND', {
      'roomId': roomId,
      'peerId': socket.id,
      'userId': userDetails['_id'],
      'username': userDetails['username'],
      'profile': userDetails['profile'],
    });

    Fluttertoast.showToast(
      msg: "You have raised your hand.",
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.blue,
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }

  void approveSpeakRequest(String peerId, String userId) {
    socket.emit('APPROVE_SPEAK', {'roomId': roomId, 'userId': userId});
    handRaiseRequests.removeWhere((request) => request['userId'] == userId);
  }

  void rejectSpeakRequest(String peerId, String userId) {
    socket.emit('REJECT_SPEAK', {'roomId': roomId, 'userId': userId});
    handRaiseRequests.removeWhere((request) => request['userId'] == userId);
  }

  void cleanupConnections() {
    for (var peerId in peerConnections.keys) {
      peerConnections[peerId]?.close();
    }
    peerConnections.clear();

    for (var userId in remoteStreams.keys) {
      remoteStreams[userId]?.dispose();
    }
    remoteStreams.clear();

    for (var renderer in remoteRenderers.values) {
      renderer.dispose();
    }
    remoteRenderers.clear();

    if (socket != null) {
      socket.off('mute');
      socket.off('add-peer');
      socket.off('remove-peer');
      socket.off('ice-candidate');
      socket.off('session-description');
      socket.off('RAISE_HAND_DUPLICATE');
      socket.off('mute');
      socket.off('unmute');
      socket.emit('leave', {'roomId': roomId});
      socket.disconnect();
    }
  }

  @override
  void onClose() async {
    cleanupConnections();
    await localRenderer.dispose();  // Clean up localRenderer here
    super.onClose();
  }
}
